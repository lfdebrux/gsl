/*
 *   Copyright (C) 2012 Philip Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */

#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include "multimin.h"
#include "_cgo_export.h"

gsl_multimin_function_fdf
wrapper_function (int n, void *p)
{
	gsl_multimin_function_fdf f_new;
	f_new.f = (double (*) (const gsl_vector  * x, void * params)) &fevaluate;
	f_new.df = (void (*) (const gsl_vector * x, void * params,gsl_vector * df)) &dfevaluate;
	f_new.fdf = (void (*) (const gsl_vector * x, void * params,double *f,gsl_vector * df)) &fdfevaluate;
	f_new.n = n;
	f_new.params = p;
	return f_new;
}
